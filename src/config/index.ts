const dbUri = process.env.MONGO_DB_URI || "mongodb://localhost/test";
const port = process.env.PORT || process.env.port || 3005;
const config = {
  dbUri,
  port
};
export default config;
