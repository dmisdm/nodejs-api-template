import * as mongoose from 'mongoose';

import config from '../config';

(mongoose as any)['Promise'] = global.Promise;

export const connectMongo = () => mongoose.connect(config.dbUri);
