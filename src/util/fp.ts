abstract class Maybe<T> {
    constructor(public value?: T) { }
    get = () => this.value;
    static fromNullable = <T>(value: T) => value ? new Some<T>(value) : new None<T>()

    abstract map<Out>(mapper: (v: T) => Out): Maybe<Out>
    abstract flatMap<Out>(mapper: (v: T) => Maybe<Out>): Maybe<Out>
}

class None<T> extends Maybe<T> {
    map<Out>(mapper: (v: T) => Out) { return new None<Out>() }
    flatMap<Out>(mapper: (v: T) => Maybe<Out>) { return new None<Out>() }
}

class Some<T> extends Maybe<T>  {
    map<Out>(mapper: (v: T) => Out) { return new Some(mapper(this.value as T)) }
    flatMap<Out>(mapper: (v: T) => Maybe<Out>) { return mapper(this.value as T) }
}


let out = Maybe.fromNullable(5)
    .map(value => value + 1)
    .flatMap(() => Maybe.fromNullable(5))
out
