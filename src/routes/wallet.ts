import * as express from 'express';
import { WalletDAO, WalletDTO } from "../db/Wallet";
import { v4 } from 'uuid';
export const WalletRouter = express.Router();

WalletRouter.get('/', (req, res, next) => {
    WalletDAO.find().exec()
        .then(data => res.json(data))
        .catch(next)
})
    .post('/', (req, res, next) => {
        new WalletDAO({
            ...req.body,
            accountId: req.headers['x-auth-token']
        }).save()
            .then(() => res.send())
            .catch(next);
    })
    .delete('/:id', ({ params }, res, next) => (
        WalletDAO.findById(params.id).remove().exec().then(() => res.status(200).send()).catch(next)
    ))
