import { authMiddleware } from './auth';
import { Express } from "@types/express";
import * as bodyParser from 'body-parser';
export const applyMiddleware = (app: Express) => {
    app.use(authMiddleware);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
}