const path = require("path");
const fs = require("fs");
module.exports = {
  entry: "./src/index.ts",
  target: "node",
  output: {
    path: path.join(__dirname, "build"),
    filename: "index.js"
  },
  resolve: {
    extensions: [".ts"]
  },
  module: {
    rules: [
      {
        test: /(\.ts$)/,
        loader: "ts-loader",
        include: /src/
      },
      {
        test: /(\.wsdl$)/,
        loader: "raw-loader",
        include: /src/
      }
    ]
  },
  externals: fs.readdirSync("node_modules").reduce(function(acc, mod) {
    if (mod === ".bin") {
      return acc;
    }

    acc[mod] = "commonjs " + mod;
    return acc;
  }, {}),

  // We are outputting a real node app!
  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false
  },
  stats: "errors-only"
};
