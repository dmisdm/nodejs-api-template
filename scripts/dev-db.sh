#!/bin/bash
# Remove the mongo container and start a new one
docker rm -f mongo
docker run -p 27017:27017 -d --name mongo -v ~/mongo-data:/data mongo --storageEngine wiredTiger