import * as mongoose from 'mongoose';
import { Schema } from "mongoose";
export const NAME = 'Wallet';
export const WalletDTO = new Schema({
    id: String,
    balance: Number,
    ccy: String,
    accountId: String
}, {
        strict: false
    });
export const WalletDAO = mongoose.model(NAME, WalletDTO);
