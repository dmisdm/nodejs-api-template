import app from "./app";
import { connectMongo } from "./db";
import { Observable } from "rxjs";
import config from "./config";

const maxRetryTimes = 50;

const retryInterval = Observable.interval(1000);
Observable.of(null)
  .do(() => console.log("Connecting to mongo..."))
  .flatMap(connectMongo)
  .retryWhen(errors =>
    errors
      .do(e => console.error(`${e}`))
      .switchMapTo(retryInterval)
      .scan(acc => acc + 1)
      .switchMap(
        v =>
          v > maxRetryTimes
            ? Observable.throw("Retried too many times")
            : Observable.of(v)
      )
      .do(v => console.log(`Retrying #${v}`))
  )
  .flatMap(
    () =>
      new Promise(resolve => {
        app.listen(config.port, () => resolve(app));
      })
  )
  .subscribe(
    () => console.log(`Listening on ${config.port}`),
    e => {
      console.error(e);
      process.abort();
    }
  );
