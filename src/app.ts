import * as express from 'express';
import { applyMiddleware } from "./middleware/apply";
import { WalletRouter } from "./routes/wallet";
const app = express();

applyMiddleware(app);

app.use('/wallet', WalletRouter);
export default app;