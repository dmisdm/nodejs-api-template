import { RequestHandler } from "@types/express";

class AuthContext {
    constructor(public username: string) { }
}

export const authMiddleware: RequestHandler = (req: any, res, next) => {
    if (!req.headers['x-auth-token']) return res.status(401).send("Unauthenticated")
    req['authContext'] = new AuthContext(req.headers['x-auth-token']);
    next();
}